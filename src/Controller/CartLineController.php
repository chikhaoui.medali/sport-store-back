<?php
namespace App\Controller;

use App\Entity\CartLine;
use App\Entity\Customer;
use App\Entity\Order;
use App\Repository\CartLineRepository;
use App\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

class CartLineController extends ApiController
{
    /**
     * @Rest\View()
     * @Rest\Get("/admin/cart-lines", name="get_orders")
     */
    public function getCartLines(CartLineRepository $repository)
    {
        $cartLines = $repository->getCartLines();

        return $repository->transformAll($cartLines);
    }
}