<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

class CategoryController extends ApiController
{
    /**
     * @Rest\View(serializerGroups={"category"})
     * @Rest\Get("/categories", name="get_categories")
     */
    public function getCategories(CategoryRepository $repository)
    {
        $categories = $repository->findAll();
        return $categories;
    }
}
