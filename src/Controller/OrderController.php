<?php
namespace App\Controller;

use App\Entity\CartLine;
use App\Entity\Customer;
use App\Entity\Order;
use App\Repository\CartLineRepository;
use App\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

class OrderController extends ApiController
{
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/save", name="save_order")
     */
    public function save(Request $request, OrderRepository $orderRepository, CartLineRepository $cartLineRepository)
    {
        $cartLines = [];
        try {
            if (! $request) {
                return $this->respondValidationError('Please provide a valid request!');
            }

            if (! $request->get('name')) {
                return $this->respondValidationError('Please provide a customer!');
            }

            $cartLines = $orderRepository->save($request);
        } catch (\Exception $e) {
            $this->respondWithErrors($e->getMessage());
        }

        return $cartLineRepository->transformAll($cartLines);
    }
}