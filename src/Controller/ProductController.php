<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

class ProductController extends ApiController
{
    /**
     * @Rest\View(serializerGroups={"product"})
     * @Rest\Get("/products", name="get_products")
     */
    public function getProducts(ProductRepository $repository)
    {
        $products = $repository->findAll();
        return $products;
    }

    /**
     * @Rest\View(serializerGroups={"product"})
     * @Rest\Get("/products/{id}", name="get_one_product")
     */
    public function getProduct($id, ProductRepository $repository)
    {
        $product = $repository->find($id);
        if (empty($product)) {
            return $this->respondNotFound(['message' => 'Product not found']);
        }
        return $product;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"product"})
     * @Rest\Post("/admin/products", name="create_product")
     */
    public function save(Request $request, ProductRepository $productRepository)
    {
        try {
            if (! $request) {
                return $this->respondValidationError('Please provide a valid request!');
            }

            if (! $request->get('name')) {
                return $this->respondValidationError('Please provide a customer!');
            }

            $product = $productRepository->save($request);
        } catch (\Exception $e) {
            $this->respondWithErrors($e->getMessage());
        }

        return $product;
    }
}
