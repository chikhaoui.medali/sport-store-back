<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    private $categoryRepository;

    public function __construct(RegistryInterface $registry, CategoryRepository $categoryRepository)
    {
        parent::__construct($registry, Product::class);
        $this->categoryRepository = $categoryRepository;
    }

    public function save($data)
    {
        $category = $data->get('category');
        $category = $this->categoryRepository->find($category['id']);
        $product = new Product();
        $product->setCategory($category);
        $product->setName($data->get('name'));
        $product->setDescription($data->get('description'));
        $product->setPrice($data->get('price'));
        $this->_em->persist($product);
        $this->_em->flush();

        return $product;
    }

    public function transform(Product $product)
    {
        return [
            'id' => (int) $product->getId(),
            'name' => (string) $product->getName(),
            'category' => $this->categoryRepository->transform($product->getCategory()),
            'description' => (string) $product->getDescription(),
            'price' => (float) $product->getPrice()
        ];
    }

    public function transformAll()
    {
        $products = $this->findAll();
        $productsArray = [];

        foreach ($products as $product) {
            $productsArray[] = $this->transform($product);
        }

        return $productsArray;
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
