<?php

namespace App\Repository;

use App\Entity\CartLine;
use App\Entity\Customer;
use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    private $productRepository;
    private $customerRepository;

    public function __construct(
        RegistryInterface $registry,
        ProductRepository $productRepository,
        CustomerRepository $customerRepository
    )
    {
        parent::__construct($registry, Order::class);
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
    }

    public function transform(Order $order)
    {
        return [
            'id' => (int) $order->getId(),
            'shipped' => (boolean) $order->getShipped(),
            'created_date' => date('Y-m-d H:i', $order->getDate()->getTimestamp()),
            'customer' => $this->customerRepository->transform($order->getCustomer())
        ];
    }

    public function save($data)
    {
        $customer = new Customer();
        $customer->setName($data->get('name'));
        $customer->setAddress($data->get('address'));
        $customer->setCity($data->get('city'));
        $customer->setState($data->get('state'));
        $customer->setCountry($data->get('country'));
        $customer->setZip($data->get('zip'));
        $this->_em->persist($customer);

        $order = new Order();
        $order->setCustomer($customer);
        $this->_em->persist($order);

        $cart = $data->get('cart');
        $cartLines = [];
        foreach ($cart['lines'] as $line) {
            $product = $this->productRepository->find($line['product']['id']);
            $cartLine = new CartLine();
            $cartLine->setQuantity($line['quantity']);
            $cartLine->setOrder($order);
            $cartLine->setProduct($product);
            $this->_em->persist($cartLine);
            $cartLines[] = $cartLine;
        }
        $this->_em->flush();
        return $cartLines;
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
