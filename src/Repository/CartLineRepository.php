<?php

namespace App\Repository;

use App\Entity\CartLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CartLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartLine[]    findAll()
 * @method CartLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartLineRepository extends ServiceEntityRepository
{
    private $orderRepository;
    private $customerRepository;
    private $productRepository;

    public function __construct(
        RegistryInterface $registry,
        OrderRepository $orderRepository,
        ProductRepository $productRepository
    )
    {
        parent::__construct($registry, CartLine::class);
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
    }

    public function getCartLines()
    {
        $qb = $this->createQueryBuilder('cl')
            ->join('cl.order', 'o')->addSelect('o')
            ->join('cl.product', 'p')->addSelect('p')
            ->join('o.customer', 'c')->addSelect('c')
            ->getQuery();

        return $qb->getResult();
    }

    public function transform(CartLine $cartLine)
    {
        return [
            'id' => (int) $cartLine->getId(),
            'quantity' => (int) $cartLine->getQuantity(),
            'product' => $this->productRepository->transform($cartLine->getProduct()),
        ];
    }

    public function transformAll($cartLines)
    {
        $cartLinesArray = [];

        /** @var $cartLine CartLine*/
        foreach ($cartLines as $cartLine) {
            $cartLinesArray[(int)$cartLine->getOrder()->getId()] = $this->orderRepository->transform($cartLine->getOrder());
        }

        foreach ($cartLines as $cartLine) {
            $cartLinesArray[(int)$cartLine->getOrder()->getId()]["products"][] = $this->transform($cartLine);
        }

        return array_values($cartLinesArray);
    }

    // /**
    //  * @return CartLine[] Returns an array of CartLine objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CartLine
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
