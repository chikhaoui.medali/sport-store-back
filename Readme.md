Get a JWTToken:
curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/login_check -d '{"username":"admin","password":"admin"}'
Example of accessing secured routes:
curl -H "Authorization: Bearer [TOKEN]" http://localhost:8000/api/admin/cart-lines